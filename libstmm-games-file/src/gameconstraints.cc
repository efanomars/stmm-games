/*
 * Copyright © 2019-2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   gameconstraints.cc
 */

#include "gameconstraints.h"

#include <stmm-games/appconstraints.h>
#include <stmm-games/apppreferences.h>
#include <stmm-games/prefselector.h>

#include <cassert>
//#include <iostream>

namespace stmg
{

void GameConstraints::initFromAppConstraints(const AppConstraints& oAppConstraints) noexcept
{
	assert(oAppConstraints.isValid());
	AppConstraints& oThis = *this;
	oThis = oAppConstraints;
	// Choose All Teams In One Level as default
	m_nLevelsMin = 1;
	m_nLevelsMax = 1;
	m_nTeamsPerLevelMin = m_nTeamsMin;
	m_nTeamsPerLevelMax = m_nTeamsMin;
	// No AI per default
	m_nAIMatesPerTeamMax = 0;
	m_bAllowMixedAIHumanTeam = false;
	m_nAITeamsMin = 0;
	m_nAITeamsMax = 0;
	//
	m_nMatesPerTeamMin = 1;
	m_nPlayersMin = m_nTeamsMin;
	assert(GameConstraints::isValid());
}

bool GameConstraints::isValid() const noexcept
{
	if (! AppConstraints::isValid()) {
		return false;
	}
	if (! ((m_nLevelsMin >= 1) && (m_nLevelsMin <= m_nLevelsMax)) ) {
		return false;
	}
	if (! ((m_nTeamsPerLevelMin >= 1) && (m_nTeamsPerLevelMin <= m_nTeamsPerLevelMax)) ) {
		return false;
	}
	if (! ((m_nMatesPerTeamMin >= 1) && (m_nMatesPerTeamMin <= m_nMatesPerTeamMax)) ) {
		return false;
	}
	if (! ((m_nPlayersMin >= 1) && (m_nPlayersMin <= m_nPlayersMax)) ) {
		return false;
	}
	if (! ((m_nAITeamsMin >= 0) && (m_nAITeamsMin <= m_nAITeamsMax)) ) {
		return false;
	}
	if (m_nTeamsMax < m_nLevelsMin) {
		return false;
	}
	if (m_nTeamsMax < m_nTeamsPerLevelMin) {
		return false;
	}
	if (m_nTeamsMax < m_nAITeamsMin) {
		return false;
	}
	if (m_nPlayersMax < m_nTeamsMin * m_nMatesPerTeamMin) {
		return false;
	}
	if (m_nTeamsMax * m_nMatesPerTeamMax < m_nPlayersMin) {
		return false;
	}

	const bool bMultiLevel = (m_nLevelsMax > 1);
	const bool bMultiTeamPerLevel = (m_nTeamsPerLevelMax > 1);
	if (bMultiLevel && bMultiTeamPerLevel) {
		return false;
	}
	return true;
}

std::pair<bool, std::string> GameConstraints::isSelectedBy(const AppPreferences& oAppPreferences) const noexcept
{
	std::string sError;
	if (! GameConstraints::isValid()) {
		sError = "Invalid game constraints";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	const int32_t nTotTeams = oAppPreferences.getTotTeams();
	if (nTotTeams < m_nTeamsMin) {
		sError = "Number of teams (" + std::to_string(nTotTeams) + ") is smaller than min (" + std::to_string(m_nTeamsMin) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if (nTotTeams > m_nTeamsMax) {
		sError = "Number of teams (" + std::to_string(nTotTeams) + ") is bigger than max (" + std::to_string(m_nTeamsMax) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	const int32_t nTotPlayers = oAppPreferences.getTotPlayers();
	if (nTotPlayers < m_nPlayersMin) {
		sError = "Number of players (" + std::to_string(nTotPlayers) + ") is smaller than min (" + std::to_string(m_nPlayersMin) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if (nTotPlayers > m_nPlayersMax) {
		sError = "Number of players (" + std::to_string(nTotPlayers) + ") is bigger than max (" + std::to_string(m_nPlayersMax) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if (nTotTeams == 1) {
		// can be both bOneTeamPerLevel or bAllTeamsInOneLevel
		if (nTotPlayers < m_nMatesPerTeamMin) {
			sError = "Number of mates per team (" + std::to_string(nTotPlayers) + ") is smaller than min (" + std::to_string(m_nMatesPerTeamMin) + ")";
			return std::make_pair(false, std::move(sError)); //-----------------
		}
		if (nTotPlayers > m_nMatesPerTeamMax) {
			sError = "Number of mates per team (" + std::to_string(nTotPlayers) + ") is bigger than max (" + std::to_string(m_nMatesPerTeamMax) + ")";
			return std::make_pair(false, std::move(sError)); //-----------------
		}
		if (m_nTeamsPerLevelMin > 1) {
			sError = "Number of teams (" + std::to_string(nTotTeams) + ") is smaller than min teams per level (" + std::to_string(m_nTeamsPerLevelMin) + ")";
			return std::make_pair(false, std::move(sError)); //-----------------
		}
		if (m_nAITeamsMin > 1) {
			sError = "Number of teams (" + std::to_string(nTotTeams) + ") is smaller than min AI teams (" + std::to_string(m_nAITeamsMin) + ")";
			return std::make_pair(false, std::move(sError)); //-----------------
		}
		if (m_nLevelsMin > 1) {
			sError = "Number of levels (" + std::to_string(nTotTeams) + ") is smaller than min (" + std::to_string(m_nLevelsMin) + ")";
			return std::make_pair(false, std::move(sError)); //-----------------
		}
	} else {
		const int32_t nTotAITeams = oAppPreferences.getTotAITeams();
		if (nTotAITeams < m_nAITeamsMin) {
			sError = "Number of AI teams (" + std::to_string(nTotAITeams) + ") is smaller than min (" + std::to_string(m_nAITeamsMin) + ")";
			return std::make_pair(false, std::move(sError)); //-----------------
		}
		if (nTotAITeams > m_nAITeamsMax) {
			sError = "Number of AI teams (" + std::to_string(nTotAITeams) + ") is bigger than max (" + std::to_string(m_nAITeamsMax) + ")";
			return std::make_pair(false, std::move(sError)); //-----------------
		}
		const bool bAllTeamsInOneLevel = (m_nLevelsMax == 1);
		const int32_t nTotLevels = (bAllTeamsInOneLevel ? 1 : nTotTeams);
		assert(nTotLevels >= 1);
		for (int32_t nLevel = 0; nLevel < nTotLevels; ++nLevel) {
			const int32_t nTotLevelTeams = (bAllTeamsInOneLevel ? nTotTeams : 1);
			if (nTotLevelTeams < m_nTeamsPerLevelMin) {
				sError = "Number of level teams (" + std::to_string(nTotLevelTeams) + ") is smaller than min (" + std::to_string(m_nTeamsPerLevelMin) + ")";
				return std::make_pair(false, std::move(sError)); //-------------
			}
			if (nTotLevelTeams > m_nTeamsPerLevelMax) {
				sError = "Number of level teams (" + std::to_string(nTotLevelTeams) + ") is bigger than max (" + std::to_string(m_nTeamsPerLevelMax) + ")";
				return std::make_pair(false, std::move(sError)); //-------------
			}
			for (int32_t nTeam = 0; nTeam < nTotLevelTeams; ++nTeam) {
				const shared_ptr<AppPreferences::PrefTeam>& refPrefTeam = oAppPreferences.getTeam(bAllTeamsInOneLevel ? nTeam : nLevel);
				//oAppPreferences.getTeam(bAllTeamsInOneLevel, nLevel, nTeam);
				const int32_t nTotTeammates = refPrefTeam->getTotMates();
				if (nTotTeammates < m_nMatesPerTeamMin) {
					sError = "Number of mates (" + std::to_string(nTotTeammates) + ") is smaller than min mates per team (" + std::to_string(m_nMatesPerTeamMin) + ")";
					return std::make_pair(false, std::move(sError)); //---------
				}
				if (nTotTeammates > m_nMatesPerTeamMax) {
					sError = "Number of mates (" + std::to_string(nTotTeammates) + ") is bigger than max mates per team (" + std::to_string(m_nMatesPerTeamMax) + ")";
					return std::make_pair(false, std::move(sError)); //---------
				}
				int32_t nTotTeamAIMates = 0;
				for (int32_t nMate = 0; nMate < nTotTeammates; ++nMate) {
					const shared_ptr<AppPreferences::PrefPlayer>& refPrefPlayer = refPrefTeam->getMate(nMate);
					if (refPrefPlayer->isAI()) {
						++nTotTeamAIMates;
					}
				}
				if (nTotTeamAIMates > m_nAIMatesPerTeamMax) {
					sError = "Number of team's AI mates (" + std::to_string(nTotTeamAIMates) + ") is bigger than max AI mates per team (" + std::to_string(m_nAIMatesPerTeamMax) + ")";
					return std::make_pair(false, std::move(sError)); //---------
				}
			}
		}
	}
	if (m_refPrefSelector) {
		if (! m_refPrefSelector->select(oAppPreferences)) {
			sError = "Extra preferences selector failed";
			return std::make_pair(false, std::move(sError)); //-----------------
		}
	}
	return std::make_pair(true, std::move(sError)); //-----------------
}

std::pair<bool, std::string> GameConstraints::isCompatibleWith(const AppConstraints& oAppConstraints) const noexcept
{
	std::string sError;
	if (! GameConstraints::isValid()) {
		sError = "The game constraints itself is invalid";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if (! oAppConstraints.isValid()) {
		sError = "The app constraints is invalid";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	const int32_t nAppMinTeams = oAppConstraints.m_nTeamsMin;
	const int32_t nAppMaxTeams = oAppConstraints.getMaxTeams();
	const int32_t nAppMaxMates = oAppConstraints.getMaxTeammates();
	const int32_t nAppMaxPlayers = oAppConstraints.getMaxPlayers();
	const bool bAppAllowsAI = oAppConstraints.allowsAI();
	if (m_nTeamsMax < nAppMinTeams) {
		sError = "Max number of teams (" + std::to_string(m_nTeamsMax) + ") is smaller than app's min (" + std::to_string(nAppMinTeams) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if (m_nTeamsMin > nAppMaxTeams) {
		sError = "Min number of teams (" + std::to_string(m_nTeamsMin) + ") is bigger than app's max (" + std::to_string(nAppMaxTeams) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if (m_nLevelsMin > nAppMaxTeams) {
		sError = "Min number of levels (" + std::to_string(m_nLevelsMin) + ") is bigger than app's max teams (" + std::to_string(nAppMaxTeams) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if (m_nMatesPerTeamMin > nAppMaxMates) {
		sError = "Min number of mates per team (" + std::to_string(m_nMatesPerTeamMin) + ") is bigger than app's max (" + std::to_string(nAppMaxMates) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if (m_nPlayersMin > nAppMaxPlayers) {
		sError = "Min number of players (" + std::to_string(m_nPlayersMin) + ") is bigger than app's max (" + std::to_string(nAppMaxPlayers) + ")";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	if ((m_nAITeamsMin > 0) && !bAppAllowsAI) {
		sError = "Min number of AI teams (" + std::to_string(m_nAITeamsMin) + ") is non-zero while app forbids AI";
		return std::make_pair(false, std::move(sError)); //---------------------
	}
	return std::make_pair(true, std::move(sError));
}

	//bool GameConstraints::operator==(const GameConstraints& oGC) const
	//{
	//	if (! AppConstraints::operator==(oGC)) {
	//		return false;
	//	}
	//	if (oGC.m_nLevelsMin != m_nLevelsMin) {
	//		return false;
	//	}
	//	if (oGC.m_nLevelsMax != m_nLevelsMax) {
	//		return false;
	//	}
	//	if (oGC.m_nTeamsPerLevelMin != m_nTeamsPerLevelMin) {
	//		return false;
	//	}
	//	if (oGC.m_nTeamsPerLevelMax != m_nTeamsPerLevelMax) {
	//		return false;
	//	}
	//	if (oGC.m_nMatesPerTeamMin != m_nMatesPerTeamMin) {
	//		return false;
	//	}
	//	if (oGC.m_nPlayersMin != m_nPlayersMin) {
	//		return false;
	//	}
	//	if (oGC.m_nAITeamsMin != m_nAITeamsMin) {
	//		return false;
	//	}
	//	if (oGC.m_nAITeamsMax != m_nAITeamsMax) {
	//		return false;
	//	}
	//	return true;
	//}

} // namespace stmg
