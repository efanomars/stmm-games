/*
 * Copyright © 2020  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   thetatraitset.cc
 */

#include "traitsets/thetatraitset.h"

#include "tile.h"
#include "util/intset.h"

#include <cassert>
#include <iostream>
#include <cstdint>
#include <string>


namespace stmg
{

ThetaTraitSet::ThetaTraitSet() noexcept
: IntTraitSet()
, m_bEmptyValueInSet(true)
{
}
ThetaTraitSet::ThetaTraitSet(int32_t nValue) noexcept
: IntTraitSet(nValue)
, m_bEmptyValueInSet(false)
{
	assert((nValue >= TileTheta::THETA_MIN) && (nValue <= TileTheta::THETA_MAX));
}
ThetaTraitSet::ThetaTraitSet(int32_t nFromValue, int32_t nToValue) noexcept
: IntTraitSet(nFromValue, nToValue)
, m_bEmptyValueInSet(false)
{
	assert((nFromValue >= TileTheta::THETA_MIN) && (nFromValue <= TileTheta::THETA_MAX));
	assert((nToValue >= TileTheta::THETA_MIN) && (nToValue <= TileTheta::THETA_MAX));
}
ThetaTraitSet::ThetaTraitSet(int32_t nFromValue, int32_t nToValue, int32_t nStep) noexcept
: IntTraitSet(nFromValue, nToValue, nStep)
, m_bEmptyValueInSet(false)
{
	assert((nFromValue >= TileTheta::THETA_MIN) && (nFromValue <= TileTheta::THETA_MAX));
	assert((nToValue >= TileTheta::THETA_MIN) && (nToValue <= TileTheta::THETA_MAX));
}
ThetaTraitSet::ThetaTraitSet(const std::vector<int32_t>& aValues) noexcept
: IntTraitSet(aValues)
, m_bEmptyValueInSet(false)
{
	#ifndef NDEBUG
	for (auto& nValue : aValues) {
		assert((nValue >= TileTheta::THETA_MIN) && (nValue <= TileTheta::THETA_MAX));
	}
	#endif //NDEBUG
}
ThetaTraitSet::ThetaTraitSet(const IntSet& oIntSet, bool bWithEmptyValue) noexcept
: IntTraitSet(oIntSet)
, m_bEmptyValueInSet(bWithEmptyValue)
{
	checkValues(oIntSet);
}
ThetaTraitSet::ThetaTraitSet(IntSet&& oIntSet, bool bWithEmptyValue) noexcept
: IntTraitSet((checkValues(oIntSet), std::move(oIntSet)))
, m_bEmptyValueInSet(bWithEmptyValue)
{
}

bool ThetaTraitSet::hasEmptyValue() const noexcept
{
	return m_bEmptyValueInSet;
}
int32_t ThetaTraitSet::getTotValues() const noexcept
{
	return IntTraitSet::getTotValues() + (m_bEmptyValueInSet ? 1 : 0);
}

std::pair<bool, int32_t> ThetaTraitSet::getTraitValue(const Tile& oTile) const noexcept
{
	const TileTheta& oTG = oTile.getTileTheta();
	if (oTG.isEmpty()) {
		return std::make_pair(true, 0);
	}
	return std::make_pair(false, oTG.getThetaValue());
}
void ThetaTraitSet::setTileTraitValue(Tile& oTile, int32_t nValue) const noexcept
{
	assert((nValue >= TileTheta::THETA_MIN) && (nValue <= TileTheta::THETA_MAX));
	TileTheta& oTG = oTile.getTileTheta();
	oTG.setThetaValue(nValue);
}
void ThetaTraitSet::resetTileTraitValue(Tile& oTile) const noexcept
{
	TileTheta& oTG = oTile.getTileTheta();
	oTG.clear();
}
void ThetaTraitSet::checkValues(const IntSet&
								#ifndef NDEBUG
								oIntSet
								#endif //NDEBUG
								) noexcept
{
	#ifndef NDEBUG
	const auto nTotValues = oIntSet.size();
	for (int32_t nIdx = 0; nIdx < nTotValues; ++nIdx) {
		const int32_t nValue = oIntSet.getValueByIndex(nIdx);
		assert((nValue >= TileTheta::THETA_MIN) && (nValue <= TileTheta::THETA_MAX));
	}
	#endif
}

void ThetaTraitSet::dump(int32_t
#ifndef NDEBUG
nIndentSpaces
#endif //NDEBUG
, bool
#ifndef NDEBUG
bHeader
#endif //NDEBUG
) const noexcept
{
	#ifndef NDEBUG
	auto sIndent = std::string(nIndentSpaces, ' ');
	if (bHeader) {
		std::cout << sIndent << "ThetaTraitSet" << '\n';
	}
	if (m_bEmptyValueInSet) {
		std::cout << sIndent << "  (Empty) " << '\n';
	}
	IntTraitSet::dump(nIndentSpaces + 2, false);
	#endif //NDEBUG
}

} // namespace stmg
