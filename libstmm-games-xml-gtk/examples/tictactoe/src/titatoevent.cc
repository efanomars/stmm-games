/*
 * Copyright © 2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   titatoevent.cc
 */

#include "titatoevent.h"

#include <stmm-games/gameproxy.h>
#include <stmm-games/level.h>
#include <stmm-games/tile.h>
#include <stmm-games/util/basictypes.h>
#include <stmm-games/util/util.h>

#include <string>
#include <cassert>
#include <iostream>

namespace stmg
{

TitatoEvent::TitatoEvent(Init&& oInit) noexcept
: Event(std::move(oInit))
, m_nSeqLength(oInit.m_nSeqLength)
{
	commonInit();
}

void TitatoEvent::reInit(Init&& oInit) noexcept
{
	Event::reInit(std::move(oInit));
	m_nSeqLength = oInit.m_nSeqLength;
	commonInit();
}
void TitatoEvent::commonInit() noexcept
{
	Level& oLevel = level();
	m_eState = TITATO_STATE_ACTIVATE;
	m_nBoardW = oLevel.boardWidth();
	m_nBoardH = oLevel.boardHeight();
	m_aTheB.resize(m_nBoardW * m_nBoardH, 0);
	m_nFreePos = 0;
	m_nControllableLBId = -1;
	for (int32_t nTeam = 0; nTeam < 2; ++nTeam) {
		m_aTeamTile[nTeam].getTileColor().setColorPal(nTeam);
	}
}

void TitatoEvent::trigger(int32_t nMsg, int32_t nValue, Event* p0TriggeringEvent) noexcept
{
	Level& oLevel = level();
	const int32_t nTimer = oLevel.game().gameElapsed();
//std::cout << "TitatoEvent::trigger nTimer=" << nTimer << " tiggering=" << reinterpret_cast<int64_t>(p0TriggeringEvent) << " p=" << getPriority() << '\n';
//std::cout << "                     nMsg=" << nMsg << " nValue=" << nValue << '\n';
	switch (m_eState)
	{
		case TITATO_STATE_ACTIVATE:
		{
			m_eState = TITATO_STATE_INIT;
			if (p0TriggeringEvent != nullptr) {
				oLevel.activateEvent(this, nTimer);
				return; //------------------------------------------------------
			}
		} //fallthrough
		case TITATO_STATE_INIT:
		{
			m_eState = TITATO_STATE_RUN;
			m_nCurrentTeam = 0;
			//GameProxy& oGame = oevel.game();
			const AppPreferences& oPrefs = oLevel.prefs();
			if (oPrefs.getTotTeams() != 2) {
				oLevel.gameStatusTechnical({"Titato needs two teams"});
				return; //------------------------------------------------------
			}
			m_aTeamIsAI[0] = oPrefs.getTeam(0)->isAI();
			m_aTeamIsAI[1] = oPrefs.getTeam(1)->isAI();
			m_nTotAITeams = oPrefs.getTotAITeams();
			LevelBlock* p0Controllable = nullptr;
			if (m_nTotAITeams < 2) {
				p0Controllable = getControllableLB(nTimer);
				if (p0Controllable == nullptr) {
					return; //--------------------------------------------------
				}
			}
			if ((m_nTotAITeams == 0) || ! m_aTeamIsAI[0]) {
				// The first player is human
				m_nControllableLBId = p0Controllable->blockGetId();
				oLevel.blockSetControllable(p0Controllable, true, m_nCurrentTeam);
				// Wait for MESSAGE_SET_TILE
				return; //------------------------------------------------------
			}
			//
		} //fallthrough
		case TITATO_STATE_RUN:
		{
			NPoint oXY;
			if (m_aTeamIsAI[m_nCurrentTeam]) {
				refreshHelpers();
				if (m_nFreePos == 0) {
					oLevel.gameStatusTechnical({"Board is full"});
					return; //--------------------------------------------------
				}
				oXY = getBestPosition();
			} else {
				if (m_nControllableLBId < 0) {
					// waiting for a controllable to show up
					LevelBlock* p0Controllable = getControllableLB(nTimer);
					if (p0Controllable == nullptr) {
						return; //----------------------------------------------
					}
					// The current player is human
					m_nControllableLBId = p0Controllable->blockGetId();
					oLevel.blockSetControllable(p0Controllable, true, m_nCurrentTeam);
				}
				if ((p0TriggeringEvent == nullptr) || (nMsg != MESSAGE_SET_TILE)) {
					return; //--------------------------------------------------
				}
				oXY = Util::unpackPointFromInt32(nValue);
				if ((oXY.m_nX < 0) || (oXY.m_nX >= m_nBoardW)) {
					return; //--------------------------------------------------
				}
				if ((oXY.m_nY < 0) || (oXY.m_nY >= m_nBoardH)) {
					return; //--------------------------------------------------
				}
				if (! oLevel.boardGetTile(oXY).isEmpty()) {
					return; //--------------------------------------------------
				}
			}
			//
			oLevel.boardSetTile(oXY, m_aTeamTile[m_nCurrentTeam]);
			//
			refreshHelpers();
			const bool bWon = checkPositionWon(m_nCurrentTeam, oXY);
			if (bWon) {
				m_eState = TITATO_STATE_FINISHED;
				informListeners(LISTENER_GROUP_SET_POS, Util::packPointToInt32(oXY));
				informListeners(LISTENER_GROUP_FINISHED, m_nCurrentTeam);
				return; //------------------------------------------------------
			}
			if (m_nFreePos == 0) {
				m_eState = TITATO_STATE_FINISHED;
				informListeners(LISTENER_GROUP_SET_POS, Util::packPointToInt32(oXY));
				informListeners(LISTENER_GROUP_FINISHED, -1);
				return; //------------------------------------------------------
			}
			m_nCurrentTeam = 1 - m_nCurrentTeam;
			if (! m_aTeamIsAI[m_nCurrentTeam]) {
				LevelBlock* p0Controllable = oLevel.blocksGet(m_nControllableLBId);
				if (p0Controllable == nullptr) {
					// controllable block disappeared: try next tick
					m_nControllableLBId = -1;
					oLevel.activateEvent(this, nTimer + 1);
				} else {
					oLevel.blockSetControllable(p0Controllable, true, m_nCurrentTeam);
					// Wait for MESSAGE_SET_TILE
				}
				informListeners(LISTENER_GROUP_SET_POS, Util::packPointToInt32(oXY));
				return; //------------------------------------------------------
			}
			// next player is AI
			oLevel.activateEvent(this, nTimer + 1);
			informListeners(LISTENER_GROUP_SET_POS, Util::packPointToInt32(oXY));
		}
		break;
		case TITATO_STATE_FINISHED:
		{
		}
		break;
	}
}
LevelBlock* TitatoEvent::getControllableLB(int32_t nTimer) noexcept
{
	Level& oLevel = level();
	std::vector<LevelBlock*> aLBs = oLevel.blocksGetAll();
	LevelBlock*p0Controllable = nullptr;
	const int32_t nTotControllables = std::count_if(aLBs.begin(), aLBs.end(), [&](LevelBlock* p0LB)
	{
		const bool bC = p0LB->isPlayerControllable();
		if (bC) {
			p0Controllable = p0LB;
		}
		return bC;
	});
	if (nTotControllables > 1) {
		// there is at least one human team but too many controllables LevelBlock
		oLevel.gameStatusTechnical({"Titato needs exactly one controllable LevelBlock"});
		return nullptr; //------------------------------------------------------
	}
	if (nTotControllables == 0) {
		// no controllable block yet: try next tick
		oLevel.activateEvent(this, nTimer + 1);
		return nullptr; //------------------------------------------------------
	}
	return p0Controllable;
}

int32_t TitatoEvent::getIndexFromPos(int32_t nX, int32_t nY) noexcept
{
	return nY * m_nBoardW + nX;
}
void TitatoEvent::refreshHelpers() noexcept
{
	Level& oLevel = level();

	m_nFreePos = 0;
	for (int32_t nX = 0; nX < m_nBoardW; ++nX) {
		for (int32_t nY = 0; nY < m_nBoardH; ++nY) {
			const Tile& oTile = oLevel.boardGetTile(nX, nY);
			if (oTile.isEmpty()) {
				++m_nFreePos;
				m_aTheB[getIndexFromPos(nX, nY)] = 0;
			} else if (oTile == m_aTeamTile[0]) {
				m_aTheB[getIndexFromPos(nX, nY)] = 1;
			} else if (oTile == m_aTeamTile[1]) {
				m_aTheB[getIndexFromPos(nX, nY)] = 2;
			} else {
				m_aTheB[getIndexFromPos(nX, nY)] = 3;
			}
		}
	}
}
bool TitatoEvent::checkPositionWon(int32_t nCurrentTeam, NPoint oXY) noexcept
{
	static constexpr std::array<int32_t, 4> s_aDeltaX = {+1,  0, +1, +1};
	static constexpr std::array<int32_t, 4> s_aDeltaY = { 0, +1, +1, -1};
	//
	for (int32_t nDir = 0; nDir < 4; ++nDir) {
		int32_t nX = oXY.m_nX;
		int32_t nY = oXY.m_nY;
		int32_t nSame = 1;
		do {
			nX += s_aDeltaX[nDir];
			if ((nX < 0) || (nX >= m_nBoardW)) {
				break;
			}
			nY += s_aDeltaY[nDir];
			if ((nY < 0) || (nY >= m_nBoardH)) {
				break;
			}
			//
			const uint8_t nTile = m_aTheB[getIndexFromPos(nX, nY)];
			if (nTile != 1 + nCurrentTeam) {
				break;
			}
			++nSame;
			if (nSame >= m_nSeqLength) {
				return true; //-----------------------------------------
			}
		} while (true);
		//
		nX = oXY.m_nX;
		nY = oXY.m_nY;
		do {
			nX -= s_aDeltaX[nDir];
			if ((nX < 0) || (nX >= m_nBoardW)) {
				break;
			}
			nY -= s_aDeltaY[nDir];
			if ((nY < 0) || (nY >= m_nBoardH)) {
				break;
			}
			//
			const uint8_t nTile = m_aTheB[getIndexFromPos(nX, nY)];
			if (nTile != 1 + nCurrentTeam) {
				break;
			}
			++nSame;
			if (nSame >= m_nSeqLength) {
				return true; //-----------------------------------------
			}
		} while (true);
		//
	}
	return false;
}

NPoint TitatoEvent::getBestPosition() noexcept
{
	// return -1 if losing position, 0 if draw or  unknown, +1 if winning position
	// expects board to not be full
	std::function<int32_t(int32_t)>* p0Recurse = nullptr;
	std::function<int32_t(int32_t)> oRecurse = [&](const int32_t nCurrentTeam)
	{
		int32_t nBestValue = -2;
		for (int32_t nX = 0; nX < m_nBoardW; ++nX) {
			bool bWon = false;
			for (int32_t nY = 0; nY < m_nBoardH; ++nY) {
				uint8_t& nTile = m_aTheB[getIndexFromPos(nX, nY)];
				if (nTile == 0) {
					nTile = 1 + nCurrentTeam;
					bWon = checkPositionWon(nCurrentTeam, NPoint{nX, nY});
					if (! bWon) {
						const int32_t nVal = (*p0Recurse)(1 - nCurrentTeam);
						if (- nVal > nBestValue) {
							nBestValue = - nVal;
						}
					}
					nTile = 0;
					if (bWon) {
						nBestValue = 1;
						break;
					}
					if (nBestValue == 1) {
						bWon = true;
						break;
					}
				}
			}
			if (bWon) {
				break;
			}
		}
		if (nBestValue < -1) {
			nBestValue = 0;
		}
		return nBestValue;
	};
	p0Recurse = &oRecurse;

	const int32_t nCurrentTeam = m_nCurrentTeam;

	NPoint oBestXY;
	int32_t nBestValue = -2;
	for (int32_t nX = 0; nX < m_nBoardW; ++nX) {
		bool bWon = false;
		for (int32_t nY = 0; nY < m_nBoardH; ++nY) {
			uint8_t& nTile = m_aTheB[getIndexFromPos(nX, nY)];
			if (nTile == 0) {
				nTile = 1 + nCurrentTeam;
				bWon = checkPositionWon(nCurrentTeam, NPoint{nX, nY});
				if (! bWon) {
					const int32_t nVal = (*p0Recurse)(1 - nCurrentTeam);
					if (- nVal > nBestValue) {
						nBestValue = - nVal;
						oBestXY = NPoint{nX, nY};
					}
				}
				nTile = 0;
				if (bWon) {
					nBestValue = 1;
					oBestXY = NPoint{nX, nY};
					break;
				}
				if (nBestValue == 1) {
					bWon = true;
					break;
				}
			}
		}
		if (bWon) {
			break;
		}
	}
	if (nBestValue < -1) {
		nBestValue = 0;
	}

	return oBestXY;
}

} // namespace stmg
