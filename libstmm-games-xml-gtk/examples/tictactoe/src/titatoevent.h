/*
 * Copyright © 2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   titatoevent.h
 */

#ifndef STMG_TITATO_EVENT_H_
#define STMG_TITATO_EVENT_H_

#include <stmm-games/event.h>
#include <stmm-games/levelblock.h>
#include <stmm-games/util/basictypes.h>

#include <vector>
#include <memory>

#include <stdint.h>

namespace stmg
{

using std::shared_ptr;
using std::unique_ptr;

class TitatoEvent : public Event
{
public:
	struct Init : public Event::Init
	{
		int32_t m_nSeqLength = 3; /**< The number of identical tiles in a row needed to win. Must be &gt;= 3. */
	};
	/** Constructor.
	 * @param oInit The initialization data.
	 */
	explicit TitatoEvent(Init&& oInit) noexcept;

protected:
	/** Reinitialization.
	 * @param oInit The initialization data.
	 */
	void reInit(Init&& oInit) noexcept;
public:

	//Event
	void trigger(int32_t nMsg, int32_t nValue, Event* p0TriggeringEvent) noexcept override;

	// input
	enum {
		MESSAGE_SET_TILE = 100 /**< Sets the current player's tile at position Util::unpackPointFromInt32(nValue).
								* If the position is occupied does nothing.*/
	};
	// output
	enum {
		LISTENER_GROUP_SET_POS = 10 /**< The tile at position was set. */
	};

private:
	void commonInit() noexcept;

	LevelBlock* getControllableLB(int32_t nTimer) noexcept;
	bool checkPositionWon(int32_t nCurrentTeam, NPoint oXY) noexcept;
	NPoint getBestPosition() noexcept;
	void refreshHelpers() noexcept;
	int32_t getIndexFromPos(int32_t nX, int32_t nY) noexcept;
private:
	int32_t m_nBoardW;
	int32_t m_nBoardH;
	int32_t m_nSeqLength;

	enum TITATO_STATE
	{
		  TITATO_STATE_ACTIVATE = 0
		, TITATO_STATE_INIT = 1
		, TITATO_STATE_RUN = 2
		, TITATO_STATE_FINISHED = 3
	};
	TITATO_STATE m_eState;

	std::array<bool, 2> m_aTeamIsAI;
	std::array<Tile, 2> m_aTeamTile;
	std::vector<uint8_t> m_aTheB;
	int32_t m_nFreePos;
	int32_t m_nTotAITeams;
	int32_t m_nCurrentTeam;
	int32_t m_nControllableLBId;

private:
	TitatoEvent();
	TitatoEvent(const TitatoEvent& oSource);
	TitatoEvent& operator=(const TitatoEvent& oSource);
};

} // namespace stmg

#endif	/* STMG_TITATO_EVENT_H_ */

