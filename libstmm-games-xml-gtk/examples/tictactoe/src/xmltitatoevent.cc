/*
 * Copyright © 2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   xmltitatoevent.cc
 */

#include "xmltitatoevent.h"

#include "titatoevent.h"

#include <stmm-games-xml-game/gamectx.h>

#include <stmm-games-xml-base/xmlutil/xmlstrconv.h>
#include <stmm-games-xml-base/xmlcommonerrors.h>
#include <stmm-games-xml-base/xmlconditionalparser.h>

#include <stmm-games/event.h>
#include <stmm-games/game.h>

#include <vector>
//#include <cassert>
//#include <iostream>
#include <utility>

namespace xmlpp { class Element; }

namespace stmg
{

static const std::string s_sEventTitatoNodeName = "TitatoEvent";
static const std::string s_sEventTitatoSeqLengthAttrName = "seqLen";

XmlTitatoEventParser::XmlTitatoEventParser()
: XmlEventParser(s_sEventTitatoNodeName)
{
}
Event* XmlTitatoEventParser::parseEvent(GameCtx& oCtx, const xmlpp::Element* p0Element)
{
	return integrateAndAdd(oCtx, parseEventTitato(oCtx, p0Element), p0Element);
}
unique_ptr<Event> XmlTitatoEventParser::parseEventTitato(GameCtx& oCtx, const xmlpp::Element* p0Element)
{
	oCtx.addChecker(p0Element);
	TitatoEvent::Init oTInit;
	parseEventBase(oCtx, p0Element, oTInit);

	const auto oPairSeqLen = XmlCommonParser::getAttributeValue(oCtx, p0Element, s_sEventTitatoSeqLengthAttrName);
	if (oPairSeqLen.first) {
		const std::string& sSeqLen = oPairSeqLen.second;
		oTInit.m_nSeqLength = XmlUtil::strToNumber<int32_t>(oCtx, p0Element, s_sEventTitatoSeqLengthAttrName, sSeqLen
															, false, true, 3, false, -1);
	}

	oCtx.removeChecker(p0Element, true);

	auto refTitatoEvent = std::make_unique<TitatoEvent>(std::move(oTInit));
	return refTitatoEvent;
}

int32_t XmlTitatoEventParser::parseEventMsgName(ConditionalCtx& oCtx, const xmlpp::Element* p0Element, const std::string& sAttr
												, const std::string& sMsgName)
{
	int32_t nMsg;
	if (sMsgName == "SET_TILE") {
		nMsg = TitatoEvent::MESSAGE_SET_TILE;
	} else {
		nMsg = XmlEventParser::parseEventMsgName(oCtx, p0Element, sAttr, sMsgName);
	}
	return nMsg;
}

int32_t XmlTitatoEventParser::parseEventListenerGroupName(GameCtx& oCtx, const xmlpp::Element* p0Element, const std::string& sAttr
														, const std::string& sListenerGroupName)
{
	int32_t nListenerGroup;
	if (sListenerGroupName == "SET_POS") {
		nListenerGroup = TitatoEvent::LISTENER_GROUP_SET_POS;
	} else {
		return XmlEventParser::parseEventListenerGroupName(oCtx, p0Element, sAttr, sListenerGroupName);
	}
	return nListenerGroup;
}

} // namespace stmg
