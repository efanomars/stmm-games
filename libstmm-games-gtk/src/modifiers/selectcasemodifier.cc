/*
 * Copyright © 2019-2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   selectcasemodifier.cc
 */

#include "modifiers/selectcasemodifier.h"
#include "stdtheme.h"

#include "stdthemedrawingcontext.h"

#include <cassert>
#include <cstdint>
#include <utility>

namespace stmg { class Image; }
namespace stmg { class Tile; }

namespace Cairo { class Context; }
namespace Cairo { template <typename T_CastFrom> class RefPtr; }

namespace stmg
{

SelectCaseModifier::SelectCaseModifier(Init&& oInit) noexcept
: ContainerModifier(std::move(oInit))
{
	for (AssCase& oAssCase : oInit.m_aCases) {
		assert((oAssCase.m_nIdAss >= 0) && owner()->hasAssignId(oAssCase.m_nIdAss));
		m_aAssigns.push_back(oAssCase.m_nIdAss);
		#ifndef NDEBUG
		for (const auto& refMod : oAssCase.m_aModifiers) {
			assert(refMod);
		}
		#endif //NDEBUG
		m_aAssignContainers.push_back(std::move(oAssCase.m_aModifiers));
	}
}
void SelectCaseModifier::applyToSubModifiers(const std::function<void(StdThemeModifier&)>& oApply) noexcept
{
	for (const auto& aContainer : m_aAssignContainers) {
		for (const auto& refMod : aContainer) {
			oApply(*refMod);
		}
	}
	ContainerModifier::applyToSubModifiers(oApply);
}
StdThemeModifier::FLOW_CONTROL SelectCaseModifier::drawTile(const Cairo::RefPtr<Cairo::Context>& refCc, StdThemeDrawingContext& oDc
													, const Tile& oTile, int32_t nPlayer, const std::vector<double>& aAniElapsed) noexcept
{
	const int32_t nTotCases = static_cast<int32_t>(m_aAssigns.size());
	for (int32_t nCase = 0; nCase < nTotCases; ++nCase) {
		const int32_t nIdAss = m_aAssigns[nCase];
		shared_ptr<Image> refImage = owner()->getAssignImage(nIdAss, oTile, nPlayer);
		if (refImage) {
			Image* p0SaveImage = oDc.getSelectedImage();
			oDc.setSelectedImage(refImage.get());
			const FLOW_CONTROL eCtl = ContainerModifier::drawTile(m_aAssignContainers[nCase], refCc, oDc, oTile, nPlayer, aAniElapsed);
			oDc.setSelectedImage(p0SaveImage);
			return eCtl; //-----------------------------------------------------
		}
	}
	// Default
	return ContainerModifier::drawTile(refCc, oDc, oTile, nPlayer, aAniElapsed);
}

} // namespace stmg
