/*
 * Copyright © 2019-2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   ifelsemodifier.cc
 */

#include "modifiers/ifelsemodifier.h"

#include <stmm-games/utile/tileselector.h>

#include <cassert>
#include <utility>
#include <iostream>

namespace Cairo { class Context; }
namespace Cairo { template <typename T_CastFrom> class RefPtr; }
namespace stmg { class StdThemeDrawingContext; }
namespace stmg { class Tile; }

namespace stmg
{

IfElseModifier::IfElseModifier(Init&& oInit) noexcept
: ContainerModifier(std::move(oInit))
{
	for (auto& oCondition : oInit.m_aConditions) {
		m_aCondition.push_back(std::move(oCondition.m_refSelect));
		#ifndef NDEBUG
		for (const auto& refMod : oCondition.m_aModifiers) {
			assert(refMod);
		}
		#endif //NDEBUG
		m_aConditionContainers.push_back(std::move(oCondition.m_aModifiers));
	}
}
void IfElseModifier::applyToSubModifiers(const std::function<void(StdThemeModifier&)>& oApply) noexcept
{
	for (const auto& aContainer : m_aConditionContainers) {
		for (const auto& refMod : aContainer) {
			oApply(*refMod);
		}
	}
	ContainerModifier::applyToSubModifiers(oApply);
}
StdThemeModifier::FLOW_CONTROL IfElseModifier::drawTile(const Cairo::RefPtr<Cairo::Context>& refCc, StdThemeDrawingContext& oDc
														, const Tile& oTile, int32_t nPlayer, const std::vector<double>& aAniElapsed) noexcept
{
	const int32_t nTotConditions = m_aCondition.size();
	for (int32_t nCondition = 0; nCondition < nTotConditions; ++nCondition) {
		const auto& refSelect = m_aCondition[nCondition];
		if (refSelect->select(oTile, nPlayer)) {
			return ContainerModifier::drawTile(m_aConditionContainers[nCondition], refCc, oDc, oTile, nPlayer, aAniElapsed);
		}
	}
	// Else
	return ContainerModifier::drawTile(refCc, oDc, oTile, nPlayer, aAniElapsed);
}

} // namespace stmg
