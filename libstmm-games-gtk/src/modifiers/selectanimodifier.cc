/*
 * Copyright © 2019-2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   selectanimodifier.cc
 */

#include "modifiers/selectanimodifier.h"
#include "stdtheme.h"

#include <stmm-games/named.h>
#include <stmm-games/util/namedindex.h>

#include <cassert>
#include <cstdint>
#include <utility>

namespace stmg { class Tile; }

namespace Cairo { class Context; }
namespace Cairo { template <typename T_CastFrom> class RefPtr; }

namespace stmg
{

SelectAniModifier::SelectAniModifier(Init&& oInit) noexcept
: ContainerModifier(std::move(oInit))
, m_nTileAniId(oInit.m_nTileAniId)
{
	assert((m_nTileAniId >= 0) && (m_nTileAniId < owner()->getNamed().tileAnis().size()));
	for (AniCase& oAniCase : oInit.m_aCases) {
		CaseFromTo oCFT;
		oCFT.m_fFrom = oAniCase.m_fFrom;
		oCFT.m_bFromExcl = oAniCase.m_bFromExcl;
		oCFT.m_fTo = oAniCase.m_fTo;
		oCFT.m_bToExcl = oAniCase.m_bToExcl;
		m_aRanges.push_back(std::move(oCFT));
		#ifndef NDEBUG
		for (const auto& refMod : oAniCase.m_aModifiers) {
			assert(refMod);
		}
		#endif //NDEBUG
		m_aRangeContainers.push_back(std::move(oAniCase.m_aModifiers));
	}
}
void SelectAniModifier::applyToSubModifiers(const std::function<void(StdThemeModifier&)>& oApply) noexcept
{
	for (const auto& aContainer : m_aRangeContainers) {
		for (const auto& refMod : aContainer) {
			oApply(*refMod);
		}
	}
	ContainerModifier::applyToSubModifiers(oApply);
}
StdThemeModifier::FLOW_CONTROL SelectAniModifier::drawTile(const Cairo::RefPtr<Cairo::Context>& refCc, StdThemeDrawingContext& oDc
													, const Tile& oTile, int32_t nPlayer, const std::vector<double>& aAniElapsed) noexcept
{
	const double fAniElapsed = aAniElapsed[m_nTileAniId];
	const int32_t nTotCases = static_cast<int32_t>(m_aRanges.size());
	for (int32_t nCase = 0; nCase < nTotCases; ++nCase) {
		const CaseFromTo& oCaseFromTo = m_aRanges[nCase];
		if (oCaseFromTo.m_bFromExcl) {
			if (fAniElapsed <= oCaseFromTo.m_fFrom) {
				continue;
			}
		} else {
			if (fAniElapsed < oCaseFromTo.m_fFrom) {
				continue;
			}
		}
		if (oCaseFromTo.m_bToExcl) {
			if (fAniElapsed >= oCaseFromTo.m_fTo) {
				continue;
			}
		} else {
			if (fAniElapsed > oCaseFromTo.m_fTo) {
				continue;
			}
		}
		const FLOW_CONTROL eCtl = ContainerModifier::drawTile(m_aRangeContainers[nCase], refCc, oDc, oTile, nPlayer, aAniElapsed);
		return eCtl; //-----------------------------------------------------
	}
	// Default
	return ContainerModifier::drawTile(refCc, oDc, oTile, nPlayer, aAniElapsed);
}

} // namespace stmg
