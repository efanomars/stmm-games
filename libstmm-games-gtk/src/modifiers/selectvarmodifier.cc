/*
 * Copyright © 2019-2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   selectvarmodifier.cc
 */

#include "modifiers/selectvarmodifier.h"
#include "stdtheme.h"

#include <cassert>
#include <cstdint>
#include <utility>

namespace stmg { class Tile; }

namespace Cairo { class Context; }
namespace Cairo { template <typename T_CastFrom> class RefPtr; }

namespace stmg
{

SelectVarModifier::SelectVarModifier(Init&& oInit) noexcept
: ContainerModifier(std::move(oInit))
, m_nVariableId(owner()->getVariableIndex(oInit.m_sVariableName))
{
	assert(! oInit.m_sVariableName.empty());
	for (VarCase& oVarCase : oInit.m_aCases) {
		CaseFromTo oCFT;
		oCFT.m_nFrom = oVarCase.m_nFrom;
		oCFT.m_nTo = oVarCase.m_nTo;
		m_aRanges.push_back(std::move(oCFT));
		#ifndef NDEBUG
		for (const auto& refMod : oVarCase.m_aModifiers) {
			assert(refMod);
		}
		#endif //NDEBUG
		m_aRangeContainers.push_back(std::move(oVarCase.m_aModifiers));
	}
}
void SelectVarModifier::applyToSubModifiers(const std::function<void(StdThemeModifier&)>& oApply) noexcept
{
	for (const auto& aContainer : m_aRangeContainers) {
		for (const auto& refMod : aContainer) {
			oApply(*refMod);
		}
	}
	ContainerModifier::applyToSubModifiers(oApply);
}
StdThemeModifier::FLOW_CONTROL SelectVarModifier::drawTile(const Cairo::RefPtr<Cairo::Context>& refCc, StdThemeDrawingContext& oDc
													, const Tile& oTile, int32_t nPlayer, const std::vector<double>& aAniElapsed) noexcept
{
//std::cout << "            SelectVarModifier::drawTile()" << '\n';
	const auto oPair = oDc.getVariableValue(m_nVariableId);
	const bool bVarUndefined = ! oPair.first;
	const int32_t nVarValue = oPair.second;

	const int32_t nTotCases = static_cast<int32_t>(m_aRanges.size());
	for (int32_t nCase = 0; nCase < nTotCases; ++nCase) {
		const CaseFromTo& oCaseFromTo = m_aRanges[nCase];
		if (oCaseFromTo.m_nFrom > oCaseFromTo.m_nTo) {
			if (! bVarUndefined) {
				continue;
			}
		} else {
			if ((nVarValue < oCaseFromTo.m_nFrom) || (nVarValue > oCaseFromTo.m_nTo)) {
				continue;
			}
		}
		const FLOW_CONTROL eCtl = ContainerModifier::drawTile(m_aRangeContainers[nCase], refCc, oDc, oTile, nPlayer, aAniElapsed);
		return eCtl; //-----------------------------------------------------
	}
	// Default
	return ContainerModifier::drawTile(refCc, oDc, oTile, nPlayer, aAniElapsed);
}

} // namespace stmg
