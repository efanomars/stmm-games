/*
 * Copyright © 2019-2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   ifelsemodifier.h
 */

#ifndef STMG_IF_ELSE_MODIFIER_H
#define STMG_IF_ELSE_MODIFIER_H

#include "containermodifier.h"

#include "stdthememodifier.h"

#include <stmm-games/utile/tileselector.h>

#include <vector>
#include <memory>

#include <stdint.h>

namespace Cairo { class Context; }
namespace Cairo { template <typename T_CastFrom> class RefPtr; }
namespace stmg { class StdThemeDrawingContext; }
namespace stmg { class Tile; }

namespace stmg
{

class StdTheme;

/** If ElseIf Else tile selection conditions.
 */
class IfElseModifier : public ContainerModifier
{
public:
	struct Condition
	{
		unique_ptr<TileSelector> m_refSelect; /**< When true executes m_aModifiers. Cannot be null. */
		std::vector< unique_ptr<StdThemeModifier> > m_aModifiers; /**< The modifiers. Can be empty. */
	};
	struct LocalInit
	{
		std::vector< Condition > m_aConditions; /**< The conditions. */
	};
	struct Init : public ContainerModifier::Init, public LocalInit
	{
	};
	/** Constructor.
	 * The submodifiers used as "else" clause are passed with Init::m_aSubModifiers.
	 * @param oInit The initialization data.
	 */
	explicit IfElseModifier(Init&& oInit) noexcept;

	FLOW_CONTROL drawTile(const Cairo::RefPtr<Cairo::Context>& refCc, StdThemeDrawingContext& oDc
						, const Tile& oTile, int32_t nPlayer, const std::vector<double>& aAniElapsed) noexcept override;

	void applyToSubModifiers(const std::function<void(StdThemeModifier&)>& oApply) noexcept override;
private:
	std::vector< unique_ptr<TileSelector> > m_aCondition;
	std::vector< std::vector< unique_ptr<StdThemeModifier> > > m_aConditionContainers; // Size: m_aCondition.size()
private:
	IfElseModifier() = delete;
	IfElseModifier(const IfElseModifier& oSource) = delete;
	IfElseModifier& operator=(const IfElseModifier& oSource) = delete;
};

} // namespace stmg

#endif	/* STMG_IF_ELSE_MODIFIER_H */

