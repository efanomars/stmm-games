/*
 * Copyright © 2019-2021  Stefano Marsili, <stemars@gmx.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>
 */
/*
 * File:   containermodifier.h
 */

#ifndef STMG_CONTAINER_MODIFIER_H
#define STMG_CONTAINER_MODIFIER_H

#include "stdthememodifier.h"

#include <cairomm/refptr.h>
#include <cairomm/surface.h>

#include <memory>
#include <vector>
#include <functional>

#include <stdint.h>

namespace stmg { class StdThemeDrawingContext; }
namespace stmg { class Tile; }

namespace Cairo { class Context; }

namespace stmg
{

using std::unique_ptr;

class StdTheme;

/** Container modifier.
 * All modifiers with submodifiers must inherit from this class.
 */
class ContainerModifier : public StdThemeModifier
{
public:
	struct LocalInit
	{
		std::vector< unique_ptr<StdThemeModifier> > m_aSubModifiers; /**< The mofdifiers cannot be null. */
	};
	struct Init : public StdThemeModifier::Init, public LocalInit
	{
	};
	/** Constructor.
	 * @param oInit The initialization data.
	 */
	explicit ContainerModifier(Init&& oInit) noexcept;
	FLOW_CONTROL drawTile(const Cairo::RefPtr<Cairo::Context>& refCc
						, StdThemeDrawingContext& oDc
						, const Tile& oTile, int32_t nPlayer, const std::vector<double>& aAniElapsed) noexcept override;
	static FLOW_CONTROL drawTile(const std::vector< unique_ptr<StdThemeModifier> >& aModifiers
								, const Cairo::RefPtr<Cairo::Context>& refCc
								, StdThemeDrawingContext& oDc
								, const Tile& oTile, int32_t nPlayer, const std::vector<double>& aAniElapsed) noexcept;
	void registerTileSize(int32_t nW, int32_t nH) noexcept override;
	void unregisterTileSize(int32_t nW, int32_t nH) noexcept override;

	/** Apply a function to all the submodifiers.
	 * The base implementation applies it to Init::m_aSubModifiers but subclasses must apply
	 * to all their additional submodifiers.
	 * @param oApply The function to apply to all submodifiers.
	 */
	virtual void applyToSubModifiers(const std::function<void(StdThemeModifier&)>& oApply) noexcept;
protected:
	FLOW_CONTROL drawContainedToWorkSurface(StdThemeDrawingContext& oDc
											, const Tile& oTile, int32_t nPlayer, const std::vector<double>& aAniElapsed
											, Cairo::RefPtr<Cairo::Surface>& refSurface) noexcept;
private:
	Cairo::RefPtr<Cairo::Context> getWorkContext(int32_t nW, int32_t nH) noexcept;
private:
	std::vector< unique_ptr<StdThemeModifier> > m_aSubModifiers;
	Cairo::RefPtr<Cairo::ImageSurface> m_refWork;
private:
	ContainerModifier() = delete;
	ContainerModifier(const ContainerModifier& oSource) = delete;
	ContainerModifier& operator=(const ContainerModifier& oSource) = delete;
};

} // namespace stmg

#endif	/* STMG_CONTAINER_MODIFIER_H */

